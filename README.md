## For Server

- Type ` cd server/`
- Make sure to have a MongoDB instance running
- Copy the environment variables from .env.sample and create a .env file
- Run `npm install`
- Run `npm run dev`


## For Front

- Type ` cd front/`
- Set up a Google Cloud Platforms for Google OAuth
- Copy the environment variables from .env.sample and create a .env file
- Run `npm install`
- Run `npm start`
- Webpack is running a dev server on localhost:8080