import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './src/components/Routes'
import './style.scss'


ReactDOM.render(
    <div className='container'>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </div>,
    document.getElementById('app')
)