import { useState, useContext } from 'react'
import { useHistory } from "react-router-dom"
import ManagementContext from '../context/Management'
import axios from 'axios'


/**
 * React custom hook used for making APi calls
 * @returns function
 */
const useFetchData = () => {
    const [data, setData] = useState(null)
    const { setLoading, setAlert, setIsAuthenticated } = useContext(ManagementContext)
    const history = useHistory()

    /**
     * 
     * @param {string} method - API method
     * @param {string} path - URL path
     * @param {object} postBody - json object sent with request
     * @param {string} redirect - otional param to redirect after API call
     */
    async function sendRequest(method, path, postBody, redirect) {
        setLoading(true)
        try {
            const request = { method, url: process.env.SERVER_URL + path }
            if (postBody) {
                request.data = postBody
            }
            if (localStorage.getItem('token')) {
                request.headers = { Authorization: `Bearer ${localStorage.getItem('token')}` }
            }
            const result = await axios(request)
            setData(result.data)

            if (result.data?.response.token) {
                setIsAuthenticated(true)
                localStorage.setItem('token', result.data.response.token)
            }

            if (redirect) {
                history.replace(redirect)
            }

            setAlert({ message: 'success', type: 'success' })
        } catch (e) {
            setAlert({ message: e.response?.data.message || 'There was an error making the request', type: 'error' })
            console.log(e.response)
        }
        finally {
            setLoading(false)
        }
    }
    return { sendRequest }
}

export default useFetchData
