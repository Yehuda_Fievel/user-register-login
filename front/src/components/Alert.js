import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'antd'


/**
 * Show system alert messages
 * @param {object} props
 * @returns React node
 */
const SystemAlert = ({ alert, setAlert }) => (
    <Alert
        message={alert.message}
        type={alert.type}
        onClose={() => setAlert(null)}
        showIcon
        closable
    />
)

SystemAlert.propTypes = {
    alert: PropTypes.object.isRequired,
    setAlert: PropTypes.func.isRequired,
}

export default SystemAlert
