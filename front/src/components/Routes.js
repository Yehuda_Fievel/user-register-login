import React, { useState } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import ManagementContext from '../context/Management'
import Alert from './Alert'
import Spinner from './Spinner'
import RegisterLogin from './RegisterLogin'
import Register from './Register'
import Login from './Login'
import Welcome from './Welcome'


/**
 * Routes container
 * @returns React node
 */
const Routes = () => {
    const [loading, setLoading] = useState(null)
    const [alert, setAlert] = useState(null)
    const [isAuthenticated, setIsAuthenticated] = useState(localStorage.getItem('token') || false)
    const value = { loading, setLoading, alert, setAlert, isAuthenticated, setIsAuthenticated }

    return (
        <ManagementContext.Provider value={value}>
            {loading && <Spinner />}
            {alert && <Alert alert={alert} setAlert={setAlert} />}
            <Switch>
                <Route exact path='/' component={RegisterLogin} />
                <Route exact path='/' component={RegisterLogin} />
                <Route exact path='/register' component={Register} />
                <Route exact path='/login' component={Login} />
                <ProtectedRoute isAuthenticated={isAuthenticated} path='/welcome' component={Welcome} />
            </Switch>
        </ManagementContext.Provider>
    )
}

/**
 * Checks authentication for protected routes
 * @param {object} props 
 * @returns React node
 */
const ProtectedRoute = ({ component: Component, isAuthenticated, path }) => (
    <Route
        path={path}
        render={props => isAuthenticated ? <Component {...props} /> : <Redirect to='/' />}
    />
)
export default Routes
