import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import GoogleLogin from 'react-google-login'
import RegisterForm from './RegisterForm'
import useFetchData from '../hooks/useFetchData'
import ManagementContext from '../context/Management'



/**
 * Container for login and register
 * @returns React node
 */
const Register = () => {
    const { sendRequest } = useFetchData()
    const { setAlert } = useContext(ManagementContext)

    const onFinish = (method, path, postBody, redirect) => {
        sendRequest(method, path, postBody, redirect)
    }

    const handleLogin = googleData => {
        if (googleData.error) {
            setAlert({ message: 'There was an error logining into Google', type: 'error' })
            return
        }
        sendRequest('POST', '/users/auth/google', { token: googleData.tokenId }, '/welcome')
    }

    return (
        <>
            <Link to='/login'>Login</Link>
            <RegisterForm onFinish={onFinish} />
            <GoogleLogin
                clientId={process.env.GOOGLE_CLIENT_ID}
                buttonText="Register in with Google"
                onSuccess={handleLogin}
                onFailure={handleLogin}
            />
        </>
    )
}

export default Register