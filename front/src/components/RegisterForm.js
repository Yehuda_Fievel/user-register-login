import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button } from 'antd'


/**
 * Registration form
 * @param {object} props 
 * @returns React node
 */
const RegistrationForm = props => {
    const [form] = Form.useForm()

    const onFinish = (values) => {
        props.onFinish('POST', '/users/register', values, '/welcome')
    }

    return (
        <Form
            {...{ labelCol: { sm: { span: 3 }, } }}
            form={form}
            name='register'
            onFinish={onFinish}
            initialValues={{
                residence: ['zhejiang', 'hangzhou', 'xihu'],
                prefix: '86',
            }}
            scrollToFirstError
        >
            <Form.Item
                name='email'
                label='E-mail'
                rules={[
                    {
                        type: 'email',
                        message: 'The input is not valid E-mail!',
                    },
                    {
                        required: true,
                        message: 'Please input your E-mail!',
                    },
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name='password'
                label='Password'
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                name='confirmPassword'
                label='Confirm Password'
                dependencies={['password']}
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: 'Please confirm your password!',
                    },
                    ({ getFieldValue }) => ({
                        validator(_, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve()
                            }
                            return Promise.reject(new Error('The two passwords that you entered do not match!'))
                        },
                    }),
                ]}
            >
                <Input.Password />
            </Form.Item>
            <Form.Item >
                <Button type='primary' htmlType='submit'>Register</Button>
            </Form.Item>
        </Form>
    )
}

RegistrationForm.propTypes = {
    onFinish: PropTypes.func.isRequired,
}

export default RegistrationForm