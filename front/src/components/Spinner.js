import React from 'react'
import { Spin } from 'antd'


/**
 * Loader to run while making API requests
 * @returns React node
 */
const Spinner = () => (
    <div className='spinner-container'>
        <div className='spinner'>
            <Spin size='large' />
        </div>
    </div>
)


export default Spinner
