import React from 'react'
import PropTypes from 'prop-types'
import { Form, Input, Button } from 'antd'


/**
 * Login form
 * @param {object} props 
 * @returns React node
 */
const LoginForm = props => {
    const onFinish = (values) => {
        props.onFinish('POST', '/users/login', values, '/welcome')
    }

    return (
        <Form
            {...{ labelCol: { sm: { span: 3 }, } }}
            name='login'
            onFinish={onFinish}
        >
            <Form.Item
                name='email'
                label='E-mail'
                rules={[
                    {
                        required: true,
                        message: 'Please input your E-mail!',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name='password'
                label='Password'
                rules={[
                    {
                        required: true,
                        message: 'Please input your Password!',
                    },
                ]}
            >
                <Input type='password' placeholder='Password' />
            </Form.Item>

            <Form.Item>
                <Button type='primary' htmlType='submit'>Login</Button>
            </Form.Item>
        </Form>
    )
}

LoginForm.propTypes = {
    onFinish: PropTypes.func.isRequired,
}

export default LoginForm