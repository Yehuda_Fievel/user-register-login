import React, { useContext } from 'react'
import GoogleLogin from 'react-google-login'
import { Tabs } from 'antd'
import Register from './RegisterForm'
import Login from './LoginForm'
import useFetchData from '../hooks/useFetchData'
import ManagementContext from '../context/Management'

const { TabPane } = Tabs


/**
 * Container for login and register
 * @returns React node
 */
const RegisterLogin = () => {
	const { sendRequest } = useFetchData()
	const { setAlert } = useContext(ManagementContext)

	const onFinish = (method, path, postBody, redirect) => {
		sendRequest(method, path, postBody, redirect)
	}

	const handleLogin = googleData => {
		if (googleData.error) {
			setAlert({ message: 'There was an error logining into Google', type: 'error' })
			return
		}
		sendRequest('POST', '/users/auth/google', { token: googleData.tokenId }, '/welcome')
	}

	return (
		<>
			<Tabs defaultActiveKey='1' >
				<TabPane tab='Register' key='1'>
					<Register onFinish={onFinish} />
				</TabPane>
				<TabPane tab='Login' key='2'>
					<Login onFinish={onFinish} />
				</TabPane>
			</Tabs>
			<GoogleLogin
				clientId={process.env.GOOGLE_CLIENT_ID}
				buttonText="Log in with Google"
				onSuccess={handleLogin}
				onFailure={handleLogin}
			/>
		</>
	)
}

export default RegisterLogin