const User = require('../models/users');
const helpers = require('../utils/helpers');
const ErrorHandler = require('../utils/errorHandler');

/**
 * Controller to register new user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function register(req, res, next) {
    try {
        let user = await User.create({ ...req.body, email: req.body.email });
        res.status(201).json({ response: { ...user, token: helpers.createJWT(user) }, message: 'User created' });
    } catch (e) {
        next(e);
    }
}


/**
 * Controller to login a user
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function login(req, res, next) {
    try {
        req.user = await User.findOne({ email: req.body.email });
        if (!req.user) {
            throw new ErrorHandler(404, 'user not found');
        }
        const isVerify = req.user.verifyPasswordSync(req.body.password);
        if (!isVerify) {
            throw new ErrorHandler(401, 'cannot verify user');
        }
        next();
    } catch (e) {
        next(e);
    }
}


/**
 * Controller to find user after Google OAuth
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
async function googleAuth(req, res, next) {
    try {
        req.user = await User.findOneAndUpdate({ email: req.profile.email }, { email: req.profile.email }, { upsert: true });
        if (!req.user) {
            throw new ErrorHandler(404, 'user not found');
        }
        next();
    } catch (e) {
        next(e);
    }
}


/**
 * Controller to send user response
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function sendUser(req, res, next) {
    try {
        res.status(200).json({ response: { ...req.user, token: helpers.createJWT(req.user) }, message: 'User logged in' });
    } catch (e) {
        next(e);
    }

}


module.exports = {
    register,
    login,
    googleAuth,
    sendUser,
}