const jwt = require('jsonwebtoken');


/**
 * Safely JSON parses a string to avoid throwing and error
 * @param {string} data
 */
function safeJsonParse(data) {
    try {
        if (data instanceof Error) {
            if (typeof data.message == 'string') {
                return data.message;
            }
            return JSON.parse(data.message);
        }
        return JSON.parse(data);
    } catch (e) {
        return data;
    }
}


/**
 * Create JWT token
 * @param {object} user
 * @param {boolean} admin
 */
function createJWT(user, admin) {
    const sign = {
        iss: 'server',
        _id: user._id,
    }
    if (admin) {
        sign.admin = true;
    }
    return jwt.sign(sign, process.env.JWT_SECRET);
}

module.exports = {
    safeJsonParse,
    createJWT,
}