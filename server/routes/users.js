const express = require('express');
const router = express.Router();
const { OAuth2Client } = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)
const schema = require('../schema/users');
const userCtrl = require('../controllers/users');



async function googleVerifyToken(req, res, next) {
    try {
        const ticket = await client.verifyIdToken({
            idToken: req.body.token,
            audience: process.env.CLIENT_ID
        });
        req.profile = ticket.getPayload();
        next()
    } catch (e) {
        next(e);
    }
}



router.post('/register', schema.register, userCtrl.register);

router.post('/login', schema.login, userCtrl.login, userCtrl.sendUser);

router.post('/auth/google', schema.googleAuth, googleVerifyToken, userCtrl.googleAuth, userCtrl.sendUser);

// router.get('/auth/google/callback', userCtrl.googleAuthCallback);


module.exports = router