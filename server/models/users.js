const mongoose = require('mongoose');
const bcrypt = require('mongoose-bcrypt');


const User = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        index: true,
    },
}, { timestamps: true });

User.plugin(bcrypt);


module.exports = exports = mongoose.model('User', User, 'users')