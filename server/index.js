require('dotenv').config()
const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = process.env.PORT || 3000;
const app = express();


console.log(`NODE_ENV: ${process.env.NODE_ENV}`);

// connect to db
require('./db');

app.use(helmet());
app.use(bodyParser.json());
app.use(cors());

// get all routes
require('./routes')(app);


process.on('unhandledRejection', error => {
    console.log('unhandledRejection: ' + error.message);
    console.log(error);
});

process.on('uncaughtException', (err, origin) => {
    console.log(`Exception origin: ${origin}`);
    console.log(`Caught exception: ${err}`);
});


app.listen(port, () => console.log(`App is listening on port ${port}!`));