const Joi = require('joi');


/**
 * 
 * @param {object} param0.schema - Joi schema for validating post body or query string
 * @param {object} param0.body - post body or query string
 * @param {object} param0.res - Express res object
 * @param {function} param0.next - Express res function
 * @returns 
 */
function validate({ schema, body, res, next }) {
    const { error, value } = schema.validate(body);
    if (error) {
        console.log(error);
        return res.status(400).json({ error: true, message: error.message });
    }
    next();
}


/**
 * Joi validation for register route
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function register(req, res, next) {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
        confirmPassword: Joi.string().valid(Joi.ref('password')).required(),
    });
    validate({ schema, body: req.body, res, next });
}


/**
 * Joi validation for login route
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function login(req, res, next) {
    const schema = Joi.object().keys({
        email: Joi.string().email().required(),
        password: Joi.string().required(),
    });
    validate({ schema, body: req.body, res, next });
}


/**
 * Joi validation for Google OAuth route
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function googleAuth(req, res, next) {
    const schema = Joi.object().keys({
        token: Joi.string().required(),
    });
    validate({ schema, body: req.body, res, next });
}


module.exports = {
    register,
    login,
    googleAuth,
}